#Развертывание Laravel приложения на VPS с помощью Laradock

**Подключаемся по ssh**

ssh root@212.237.31.186

Устанавливаем git

В корневой директории:

git clone https://gitlab.com/PetrovichevSergey/blog.git

cd blog

git submodule add https://github.com/Laradock/laradock.git

cd laradock

cp env-example .env

Устанавливаем Docker-CE и Docker-compose, по документации.

https://docs.docker.com/install/linux/docker-ce/debian/#install-docker-ce-1

https://docs.docker.com/compose/install/

правим /etc/hosts (nano /etc/hosts)

добавить строку 127.0.0.1 projectname.local

docker-compose up -d nginx php-fpm mariadb workspace

Запустятся контейнеры, после этого можно будет получить доступ внутри контейнеров:

docker-compose exec workspace bash

cp .env.example .env

nano .env

DB_HOST=mysql

DB_USERNAME=root

DB_PASSWORD=root

DB_DATABASE=blog

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"

php composer-setup.php

php -r "unlink('composer-setup.php');"

composer install

exit

docker-compose exec mariadb bash

mysql -uroot -proot

CREATE DATABASE IF NOT EXISTS `blog` COLLATE 'utf8_general_ci' ;

exit

exit

docker-compose up -d nginx php-fpm mariadb workspace

docker-compose exec workspace bash


		После установки:

		composer install --no-dev

		php artisan key:generate

		php artisan migrate

		php artisan vendor:publish

		chmod 777 -R storage

		Laravel Passport`
		php artisan passport:install
		php artisan vendor:publish --tag=passport-components
		npm run dev
		php artisan passport:keys
		php artisan passport:client

		Выполнить исправление https://github.com/Pingvi/SleepingOwlAdmin/commit/8d3c9d0d17021bed8c8fc5665e13918ca011cf03

###Не забыть поправить файл projectname/laradock/nginx/sites/projectname.conf Там должен быть поправленый projectname/laradock/nginx/sites/laravel.conf.example там требуется заменить в пути Laravel на projectname###

меняем там две строки:

	server_name projectname.local;
	
    root /var/www/projectname/public;

PhpMyAdmin start:

Чтобы использовть с mariadb поправьте .env и измените значение переменной PMA_DB_ENGINE=mysql на PMA_DB_ENGINE=mariadb.

docker-compose up -d phpmyadmin

docker-compose stop phpmyadmin

http://localhost:8080

http://laradock.io/documentation/#use-phpmyadmin

Памятка по docker:

# Запуск нескольких контейнеров

docker-compose up -d nginx php-fpm mariadb workspace

# Посмотреть список контейнеров и их статус

docker-compose ps 

# Остановить все контейнеры

docker-compose stop 

# Удалить все образы

docker-compose down 

# Пересобрать контейнер 

docker-compose build php-fpm

*Например, после смены версии php*

# Посмотреть последние строки лога контейнера

docker logs laradock_mysql_1

# Войти в конейтнер workspace под обычным пользователем

docker-compose exec --user=laradock workspace bash 

