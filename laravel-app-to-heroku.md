#1. Установим heroku-cli, например для arch linux этот пакет доступен в AUR, также необходим аккаунт на heroku.

   Проверим работу, посмотрим версию heroku-cli

   heroku --version

#2. Получаем свой проект из репозитория.
#3. Переходим в директорию проекта.
#4. Логинимся в heroku-cli

   heroku login

#5. Создаем Procfile и вносим в него изменения, устанавливаем композером установку необходимых пакетов.

   touch Procfile

   echo web: vendor/bin/heroku-php-apache2 public/ > Procfile

   composer install

#6. Создаем на сайте новое приложение. Инициализируем git-репозиторий, добавляем все в индекс, коммитим:

    heroku git:remote -a petrovichev-blog

    git add .

    git commit -m "Initial commit"

#7. Создаем приложение на heroku

    git push heroku master

#8. Откроем приложение в браузере:

    heroku open

#9. Включим дебаг на сервере, чтобы менять переменные окружения. Зайдите на вкладку settings в панели вашего приложения. Разверните форму просмотра и добавления переменных нажав кнопку: “Reveal config vars”

   Установите:

   APP_DEBUG = true

#10. Обновив страницу мы увидим, что у нас не сгенерирован код приложения.

#11. Исправим это выполнив:

heroku config:set APP_KEY=$(php artisan --no-ansi key:generate --show)

#12. Обновим теперь страницу в браузере и увидим, что теперь у нас проблемы с доступом к БД. Которой, кстати, у нас еще нет.

#13. Также нам понадобиться включить загрузку только по https://

   Это обязательно.

   В App/Providers/AppServiceProvider.php проекта в методе boot() добавим следующее:

        use Illuminate\Support\Facades\URL;

        ...

        // Force SSL in production
        if ($this->app->environment() == 'production') {
            URL::forceScheme('https');
        }

#14. Добавим изменения в репозиторий:

    git add .

    git commit -m 'heroku deployment stuff'

    git push heroku master

#15. Установим в приложение аддон с PostgreSQL

    heroku addons:create heroku-postgresql:hobby-dev

    Автоматически установиться переменная окружения DATABASE_URL

#16. Добавим изменения в конфигурационный файл config/database.php

  $DATABASE_URL = parse_url(getenv("DATABASE_URL"));

  return [
      // …

      'default' => env('DB_CONNECTION', 'pgsql_production'),

      // …

      'connections' => [

          // …

          'pgsql_production' => [
              'driver' => 'pgsql',
              'host' => $DATABASE_URL["host"],
              'port' => $DATABASE_URL["port"],
              'database' => ltrim($DATABASE_URL["path"], "/"),
              'username' => $DATABASE_URL["user"],
              'password' => $DATABASE_URL["pass"],
              'charset' => 'utf8',
              'prefix' => '',
              'schema' => 'public',
              'sslmode' => 'require',
          ],

          // …

      ],

      // …

  ];

#18. Добавим изменения в репозиторий:

    git add .

    git commit -m 'database settings'

    git push heroku master

#20. Запустим миграцию БД.

    heroku run php /app/artisan migrate